Herbivores
* Horse
* Cow
* Sheep
* Chicken
* Capybara

Carnivores
* Polar Bear
* Lion
* Cat
* Frog
* Hawk

Omnivores
* Dog
* Rat
* Fox
* Raccoon
* Fennec Fox